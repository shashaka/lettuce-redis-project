package org.blog.test;

import com.lambdaworks.redis.RedisClient;
import com.lambdaworks.redis.api.StatefulRedisConnection;
import com.lambdaworks.redis.api.sync.RedisCommands;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@Slf4j
public class LettuceApplication implements CommandLineRunner {
    public static void main(String[] args) {
        SpringApplication.run(LettuceApplication.class);
    }

    @Autowired
    private RedisClient redisClient;

    @Override
    public void run(String... args) throws Exception {
        StatefulRedisConnection<String, String> connection = redisClient.connect();
        RedisCommands<String, String> syncCommands = connection.sync();

        syncCommands.set("key", "Hello, Redis!");

        String result = syncCommands.get("key");

        log.info("result : {}", result);

        connection.close();
        redisClient.shutdown();
    }
}
